---
layout: page
title: About
permalink: /about/
---

Java研发工程师，专注于分布式系统中间件的研发。熟悉python和golang语言。

个人GitHub: 
[EdgarTeng][EdgarTeng-github]


[EdgarTeng-github]: https://github.com/EdgarTeng
